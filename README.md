Automation Logic Tech Test:

Creating VMs/Loadbalancer using Vagrant and configuring using Ansible

How to run:
  - Once in the directory
        - Run 'Vagrant up'

  - Once completed
        - Run 'ruby testlb.rb -u "10.0.15.11" -n 100' (this is to test the loadbalancer)

  - The ip addresses should be:
      - load balancer: 10.0.15.11
      - web 1 : 10.0.15.21
      - web 2 : 10.0.15.22

Extra:

  - Sometimes the loadbalancer may not be created when using 'Vagrant up' the first time. If this happens
  re-run the 'Vagrant up' command one more time
  - run 'Vagrant provision' command to make sure it is all provisioned
